﻿using ProdajtaStanova.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProdajtaStanova.Web.Controllers
{
    public class AdministratorController : Controller
    {
        // GET: Administrator
        public ActionResult Index()
        {
            User user = (User)Session["User"];
            if (user == null || user.Username.Equals(""))
                return RedirectToAction("Index", "Authentication");

            ViewBag.User = user;
            Dictionary<int, User> users = (Dictionary<int, User>)HttpContext.Application["Users"];
            return View(users);
        }
        public ActionResult DeleteUser(int UserID)
        {
            User user = (User)Session["User"];
            if (user == null || user.Username.Equals(""))
                return RedirectToAction("Index", "Authentication");


            Dictionary<int, User> users = (Dictionary<int, User>)HttpContext.Application["Users"];

            foreach (User item in users.Values)
            {
                if (item.ID == UserID)
                {
                    item.Deleted = true;
                }
            }

            Data.SaveUsers(users);
            ViewBag.User = user;
            
            return View("Index", users);
        }
        [HttpGet]
        public ActionResult AddApartment()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddApartment(Apartment apartment)
        {
            Dictionary<int, Apartment> apartments = (Dictionary<int, Apartment>)HttpContext.Application["Apartments"];
            foreach (Apartment item in apartments.Values)
            {
                if (item.ID == apartment.ID)
                {
                    ViewBag.Message = $"That apartment with ID: {apartment.ID} already exists!";
                    return View();
                }
                if (apartment.ID == 0)
                {
                    ViewBag.Message = $"Apartment ID can't be 0 or empty!";
                    return View();
                }
                if (apartment.ID < 0)
                {
                    ViewBag.Message = $"Apartment ID can't be negative!";
                    return View();
                }
                if (string.IsNullOrEmpty(apartment.Country))
                {
                    ViewBag.Message = $"Country can't be empty!";
                    return View();
                }
                if (apartment.Country.Length < 3)
                {
                    ViewBag.Message = $"Country needs to be at least 3 characters long!";
                    return View();
                }
                if (string.IsNullOrEmpty(apartment.City))
                {
                    ViewBag.Message = $"City can't be empty!";
                    return View();
                }
                if (string.IsNullOrEmpty(apartment.StreetAndNumber))
                {
                    ViewBag.Message = $"Street And Number can't be empty!";
                    return View();
                }
                if (apartment.Flor < 0)
                {
                    ViewBag.Message = $"Apartment flor can't be negative or empty (at least zero for the base flor)!";
                    return View();
                }
                if (apartment.Quadrature < 0 || apartment.Quadrature == 0)
                {
                    ViewBag.Message = $"Quadrature can't be negative or zero";
                    return View();
                }
                if (string.IsNullOrEmpty(apartment.Description))
                {
                    ViewBag.Message = $"Description can't be empty!";
                    return View();
                }
                if (apartment.Type != ApartmentType.ONE_ROOM && apartment.Type != ApartmentType.TWO_ROOM && apartment.Type != ApartmentType.THREE_ROOM && apartment.Type != ApartmentType.FOUR_ROOM)
                {
                    ViewBag.Message = $"You have to pick a Type from (ONE_ROOM, TWO_ROOM, THREE_ROOM, FOUR_ROOM!";
                    return View();
                }
                if (apartment.Price < 0 || apartment.Price == 0)
                {
                    ViewBag.Message = $"Price can't be negative or zero";
                    return View();
                }

            }

            apartments.Add(apartment.ID, apartment);
            Data.SaveApartments(apartments);
            return RedirectToAction("Index", "Home", apartments);
        }


        [HttpGet]
        public ActionResult EditApartment(int id)
        {
            User user = (User)Session["User"];
            if (user == null || user.Username.Equals(""))
                return RedirectToAction("Index", "Authentication");


            Dictionary<int, Apartment> apartments = (Dictionary<int, Apartment>)HttpContext.Application["Apartments"];
            foreach (Apartment apartment in apartments.Values)
            {
                if (apartment.ID == id && user.Role == UserType.ADMINISTRATOR)
                {
                    ViewBag.Message = TempData["message"];
                    return View(apartment);
                }
            }
            return Content("Appartment doesn't exist!");
        }


        public ActionResult SaveEdited(Apartment apartment)
        {

            User user = (User)Session["User"];
            if (user == null || user.Username.Equals(""))
                return RedirectToAction("Index", "Authentication");



            Dictionary<int, Apartment> apartments = (Dictionary<int, Apartment>)HttpContext.Application["Apartments"];
            foreach (int key in apartments.Keys)
            {
                if(key == apartment.ID && user.Role == UserType.ADMINISTRATOR)
                {
                    if (string.IsNullOrEmpty(apartment.Country))
                    {
                        TempData["message"] = $"Country can't be empty!";
                        return RedirectToAction("EditApartment", "Administrator", new { id = apartment.ID });
                    }
                    if (apartment.Country.Length < 3)
                    {
                        TempData["message"] = $"Country needs to be at least 3 characters long!";
                        return RedirectToAction("EditApartment", "Administrator", new { id = apartment.ID });
                    }
                    if (string.IsNullOrEmpty(apartment.City))
                    {
                        TempData["message"] = $"City can't be empty!";
                        return RedirectToAction("EditApartment", "Administrator", new { id = apartment.ID });
                    }
                    if (string.IsNullOrEmpty(apartment.StreetAndNumber))
                    {
                        TempData["message"] = $"Street And Number can't be empty!";
                        return RedirectToAction("EditApartment", "Administrator", new { id = apartment.ID });
                    }
                    if (apartment.Flor < 0)
                    {
                        TempData["message"] = $"Apartment flor can't be negative or empty (at least zero for the base flor)!";
                        return RedirectToAction("EditApartment", "Administrator", new { id = apartment.ID });
                    }
                    if (apartment.Quadrature < 0 || apartment.Quadrature == 0)
                    {
                        TempData["message"] = $"Quadrature can't be negative or zero";
                        return RedirectToAction("EditApartment", "Administrator", new { id = apartment.ID });
                    }
                    if (string.IsNullOrEmpty(apartment.Description))
                    {
                        TempData["message"] = $"Description can't be empty!";
                        return RedirectToAction("EditApartment", "Administrator", new { id = apartment.ID });
                    }
                    if (apartment.Type != ApartmentType.ONE_ROOM && apartment.Type != ApartmentType.TWO_ROOM && apartment.Type != ApartmentType.THREE_ROOM && apartment.Type != ApartmentType.FOUR_ROOM)
                    {
                        TempData["message"] = $"You have to pick a Type from (ONE_ROOM, TWO_ROOM, THREE_ROOM, FOUR_ROOM!";
                        return RedirectToAction("EditApartment", "Administrator", new { id = apartment.ID });
                    }
                    if (apartment.Price < 0 || apartment.Price == 0)
                    {
                        TempData["message"] = $"Price can't be negative or zero";
                        return RedirectToAction("EditApartment", "Administrator", new { id = apartment.ID });
                    }

                    apartments[key] = apartment;
                    ViewBag.Apartment = apartments[key];
                    Data.SaveApartments(apartments);
                    return RedirectToAction("Details", "Home", apartments[key]);
                }
            }
            return Content("Appartment doesn't exist!");
        }

    }
}