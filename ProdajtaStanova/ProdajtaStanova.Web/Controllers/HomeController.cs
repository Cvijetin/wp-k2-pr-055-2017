﻿using ProdajtaStanova.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProdajtaStanova.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            User user = (User)Session["User"];
            if (user == null || user.Username.Equals("") || !user.LoggedIn)
                return RedirectToAction("Index", "Authentication");

            ViewBag.User = user;
            Dictionary<int, Apartment> apartments = (Dictionary<int, Apartment>)HttpContext.Application["Apartments"];
            return View(apartments);

        }

        public ActionResult Details(int id)
        {
            User user = (User)Session["User"];
            if (user == null || user.Username.Equals(""))
                return RedirectToAction("Index", "Authentication");

            Dictionary<int, Apartment> apartments = (Dictionary<int, Apartment>)HttpContext.Application["Apartments"];
            if (apartments.ContainsKey(id))
                ViewBag.Apartment = apartments[id];

            List<Purchase> purchases = (List<Purchase>)HttpContext.Application["Purchases"];
            Dictionary<int, User> users = (Dictionary<int, User>)HttpContext.Application["Users"];

            foreach (var item in purchases)
            {
                if(item.ApartmentID == apartments[id].ID)
                {
                    if (users.Keys.Contains(item.UserID))
                    {
                        ViewBag.Owner = users[item.UserID].Username;
                    }
                    else
                        ViewBag.Owner = "";
                }
            }

            ViewBag.User = user;

            return View();
        }

        public ActionResult Buy(int id)
        {
            User user = (User)Session["User"];
            if (user == null || user.Username.Equals(""))
                return RedirectToAction("Index", "Authentication");

            Dictionary<int, Apartment> apartments = (Dictionary<int, Apartment>)HttpContext.Application["Apartments"];
            if (apartments.ContainsKey(id))
                ViewBag.Apartment = apartments[id];

            List<Purchase> purchases = (List<Purchase>)HttpContext.Application["Purchases"];


            if(user.Role == UserType.CUSTOMER && apartments[id].OnSale)
            {
                
                Purchase purchase = new Purchase();
                purchase.UserID = user.ID;
                purchase.ApartmentID = apartments[id].ID;
                purchase.DateOfPurchase = DateTime.Now;
                purchase.ChargedPrice = apartments[id].Price;
                purchases.Add(purchase);
                Data.SavePurchases(purchases);
                apartments[id].OnSale = false; //ovde ga prodajem
                Data.SaveApartments(apartments);
            }

            ViewBag.User = user;
            return RedirectToAction("MyPurchases", "Home");
        }

        public ActionResult MyPurchases()
        {

            User user = (User)Session["User"];
            if (user == null || user.Username.Equals(""))
                return RedirectToAction("Index", "Authentication");

            ViewBag.User = user;

            List<Purchase> purchases = (List<Purchase>)HttpContext.Application["Purchases"];
            Dictionary<int, User> users = (Dictionary<int, User>)HttpContext.Application["Users"];
            Dictionary<int, Apartment> apartments = (Dictionary<int, Apartment>)HttpContext.Application["Apartments"];


            List<FormatedPurchase> formatedPurchases = new List<FormatedPurchase>();

            foreach (Purchase purchase in purchases)
            {
                if(user.ID == purchase.UserID && apartments.ContainsKey(purchase.ApartmentID))
                {
                    FormatedPurchase item = new FormatedPurchase
                    {
                        City = apartments[purchase.ApartmentID].City,
                        Street = apartments[purchase.ApartmentID].StreetAndNumber,
                        Quadrature = apartments[purchase.ApartmentID].Quadrature,
                        PurchaseDate = purchase.DateOfPurchase,
                        Price = apartments[purchase.ApartmentID].Price
                    };
                    formatedPurchases.Add(item);
                }
            }
            //foreach (Purchase purchase in purchases)
            //{
            //    foreach (int UserKey in users.Keys)
            //    {
            //        if(purchase.UserID == UserKey && apartments.ContainsKey(purchase.ApartmentID))
            //        {
            //            FormatedPurchase item = new FormatedPurchase
            //            {
            //                City = apartments[purchase.ApartmentID].City,
            //                Street = apartments[purchase.ApartmentID].StreetAndNumber,
            //                Quadrature = apartments[purchase.ApartmentID].Quadrature,
            //                PurchaseDate = purchase.DateOfPurchase,
            //                Price = apartments[purchase.ApartmentID].Price
            //            };
            //            formatedPurchases.Add(item);
            //        }
            //    }
            //}
            return View(formatedPurchases);
        }
    }
}