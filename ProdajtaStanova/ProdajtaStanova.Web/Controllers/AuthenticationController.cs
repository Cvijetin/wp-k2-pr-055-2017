﻿using ProdajtaStanova.Web.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ProdajtaStanova.Web.Controllers
{
    public class AuthenticationController : Controller
    {
        // GET: Authentication
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            Dictionary<int, User> users = (Dictionary<int, User>)HttpContext.Application["Users"];
            User temp = null;
            foreach (User user in users.Values)
            {
                if(user.Username.Equals(username) && user.Password.Equals(password) && user.Deleted == false)
                {
                    temp = user;
                    temp.LoggedIn = true;
                    Session["user"] = temp;
                    return RedirectToAction("Index", "Home");
                }
            }

            ViewBag.Message = $"User with credentials does not exist!";
            return View("Index");
        }

        public ActionResult Logout()
        {
            User user = (User)Session["user"];
            if(user != null)
            {
                user.LoggedIn = false;
                Session["user"] = user;
                Session["user"] = null;
            }

            return View("Index");
        }

        public ActionResult Register()
        {
            User user = new User();
            Session["user"] = user;
            return View(user);
        }
        [HttpPost]
        public ActionResult Register(User user)
        {
            Dictionary<int, User> users = (Dictionary<int, User>)HttpContext.Application["Users"];
            foreach (User item in users.Values)
            {
                if( (item.ID == user.ID) || (item.Username == user.Username))
                {
                    if (item.Role == UserType.ADMINISTRATOR)
                    {
                        ViewBag.Message = $"User can not be declared as an Administrator!";
                        return View();
                    }
                    ViewBag.Message = $"User with Username: {user.Username} or ID: {user.ID} already exists!";
                    return View();
                }
                if((user.ID == 0))
                {
                    ViewBag.Message = $"User ID can't be 0 or empty!";
                    return View();
                }
                if ((user.ID < 0))
                {
                    ViewBag.Message = $"User ID can't be negative!";
                    return View();
                }
                if (string.IsNullOrEmpty(user.Username))
                {
                    ViewBag.Message = $"Username can't be empty!";
                    return View();
                }
                if (user.Username.Length < 3)
                {
                    ViewBag.Message = $"Username needs to be at least 3 characters long!";
                    return View();
                }
                if (string.IsNullOrEmpty(user.Password))
                {
                    ViewBag.Message = $"Password can't be empty!";
                    return View();
                }
                if (user.Password.Length < 8)
                {
                    ViewBag.Message = $"Password needs to be at least 8 characters long!";
                    return View();
                }
                if (string.IsNullOrEmpty(user.Name))
                {
                    ViewBag.Message = $"Name can't be empty!";
                    return View();
                }
                if (string.IsNullOrEmpty(user.Surname))
                {
                    ViewBag.Message = $"You need to enter a Surname!";
                    return View();
                }
                if (user.Gender != Gender.MALE && user.Gender != Gender.FEMALE)
                {
                    ViewBag.Message = $"You have to pick a gender!";
                    return View();
                }
                if (string.IsNullOrEmpty(user.Email))
                {
                    ViewBag.Message = $"Email can't be empty!";
                    return View();
                }
                if (!IsValidEmail(user.Email))
                {
                    ViewBag.Message = $"You have entered a wrong email!";
                    return View();
                }
                if (!IsValidDate(user.BirthDate))
                {
                    ViewBag.Message = $"Enter the date in format: dd/mm/yyy";
                    return View();
                }
                
            }

            users.Add(user.ID, user); //doda se novi (koji nema ID/Username/Role Administratora u fajlu
            Data.SaveUsers(users);
            Session["user"] = user;
            return RedirectToAction("Index", "Home");
        }

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
        bool IsValidDate(DateTime date)
        {
            try
            {
                DateTime.Parse(date.ToString());
                //DateTime.ParseExact(date.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                return true;
            }
            catch (ArgumentNullException anex)
            {
                return false;
            }
            catch (FormatException fex)
            {
                return false;
            }
            
            
            //Regex regex = new Regex(@"(((0|1)[0-9]|2[0-9]|3[0-1])\/(0[1-9]|1[0-2])\/((19|20)\d\d))$");
            //bool isValid = regex.IsMatch(txtDate.ToString());

            //return isValid;
        }
    }
}