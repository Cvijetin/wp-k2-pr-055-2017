﻿using ProdajtaStanova.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProdajtaStanova.Web.Controllers
{
    public class UnregistredUserController : Controller
    {
        // GET: UnregistredUser
        public ActionResult Index()
        {
            Dictionary<int, Apartment> apartments = (Dictionary<int, Apartment>)HttpContext.Application["Apartments"];
            return View(apartments);
        }

        public ActionResult Sort(string sortBy)
        {
            Dictionary<int, Apartment> apartments = (Dictionary<int, Apartment>)HttpContext.Application["Apartments"];
            Dictionary<int, Apartment> temp = new Dictionary<int, Apartment>();
            if(sortBy == "Price Ascending") // uzlazni red, manje -> vece
                temp = apartments.OrderBy(x => x.Value.Price).ToDictionary(x => x.Key, x => x.Value);
            else if (sortBy == "Price Descending") //silazni red, vece -> manje
                temp = apartments.OrderByDescending(x => x.Value.Price).ToDictionary(x => x.Key, x => x.Value);
            else if (sortBy == "ApartmentType Ascending")
                temp = apartments.OrderBy(x => x.Value.Type).ToDictionary(x => x.Key, x => x.Value);
            else if (sortBy == "ApartmentType Descending")
                temp = apartments.OrderByDescending(x => x.Value.Type).ToDictionary(x => x.Key, x => x.Value);
            else if (sortBy == "City Ascending")
                temp = apartments.OrderBy(x => x.Value.City).ToDictionary(x => x.Key, x => x.Value);
            else
                temp = apartments.OrderByDescending(x => x.Value.City).ToDictionary(x => x.Key, x => x.Value);


            return View("Index", temp);
        }
        public ActionResult SearchByCity(string SearchByCity)
        {
            Dictionary<int, Apartment> apartments = (Dictionary<int, Apartment>)HttpContext.Application["Apartments"];
            Dictionary<int, Apartment> temp = new Dictionary<int, Apartment>();

            if(SearchByCity == "")
            {
                temp = apartments;
            }
            else
            {
                foreach (Apartment item in apartments.Values)
                {
                    if (item.City == SearchByCity)
                    {
                        temp.Add(item.ID, item);
                    }
                }
            }
            return View("Index", temp);
        }

        public ActionResult SearcByType(string ApartmentType)
        {
            Dictionary<int, Apartment> apartments = (Dictionary<int, Apartment>)HttpContext.Application["Apartments"];
            Dictionary<int, Apartment> temp = new Dictionary<int, Apartment>();

            if (ApartmentType == "")
            {
                temp = apartments;
            }
            else
            {
                foreach (Apartment item in apartments.Values)
                {
                    if (item.Type.ToString() == ApartmentType)
                    {
                        temp.Add(item.ID, item);
                    }
                }
            }

            return View("Index", temp);
        }
        public ActionResult SearcByPrice(int? StartPrice, int? EndPrice)
        {
            Dictionary<int, Apartment> apartments = (Dictionary<int, Apartment>)HttpContext.Application["Apartments"];
            Dictionary<int, Apartment> temp = new Dictionary<int, Apartment>();

            if (StartPrice == null && EndPrice == null)
            {
                temp = apartments;
            }
            else
            {
                foreach (Apartment item in apartments.Values)
                {
                    if (item.Price <= EndPrice && item.Price >= StartPrice)
                    {
                        temp.Add(item.ID, item);
                    }
                }
            }

            return View("Index", temp);
        }
    }
}