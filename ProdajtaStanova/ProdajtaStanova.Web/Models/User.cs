﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ProdajtaStanova.Web.Models
{
    public class User
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Unesite ime u rasponu od 3-60"), MinLength(3), MaxLength(60)]
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public Gender Gender { get; set; }
        public string Email { get; set; }
        public DateTime BirthDate { get; set; }
        public UserType Role { get; set; }
        public bool LoggedIn { get; set; }
        public bool Deleted { get; set; }

        public User()
        {
            ID = 0;
            Username = "";
            Password = "";
            Name = "";
            Surname = "";
            Gender = Gender.MALE;
            Email = "";
            Role = UserType.CUSTOMER;
            LoggedIn = false;
            Deleted = false;

        }
        public User(int iD, string username, string password, string name, string surname, Gender gender, string email, DateTime birthDate, UserType userType, bool deleted)
        {
            ID = iD;
            Username = username;
            Password = password;
            Name = name;
            Surname = surname;
            Gender = gender;
            Email = email;
            BirthDate = birthDate;
            Role = userType;
            LoggedIn = false;
            Deleted = deleted;
        }

        public override bool Equals(object obj)
        {
            return ((User)obj).ID.Equals(this.ID); //poredjenje po ID-u
        }
        public override string ToString()
        {
            return $"{ID};{Username};{Password};{Name};{Surname};{Gender.ToString()};{Email};{BirthDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)};{Role.ToString()};{Deleted.ToString()}";
        }
    }
}