﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProdajtaStanova.Web.Models
{
    public class Purchase
    {
        public int UserID { get; set; }
        public int ApartmentID{ get; set; }
        public DateTime DateOfPurchase{ get; set; }
        public double ChargedPrice{ get; set; }

        public Purchase()
        {
            UserID = 0;
            ApartmentID = 0;
            DateOfPurchase = DateTime.Now;
            ChargedPrice = 0;
        }

        public Purchase(int userID, int apartmentID, DateTime dateOfPurchase, double chargedPrice)
        {
            UserID = userID;
            ApartmentID = apartmentID;
            DateOfPurchase = dateOfPurchase;
            ChargedPrice = chargedPrice;
        }

        public override string ToString()
        {
            return $"{UserID};{ApartmentID};{DateOfPurchase};{ChargedPrice}";
        }
    }
}