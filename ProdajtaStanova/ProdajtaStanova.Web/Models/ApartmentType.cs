﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProdajtaStanova.Web.Models
{
    public enum ApartmentType
    {
        ONE_ROOM,
        TWO_ROOM,
        THREE_ROOM,
        FOUR_ROOM
    }
}