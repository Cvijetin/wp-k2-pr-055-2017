﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProdajtaStanova.Web.Models
{
    public class FormatedPurchase
    {
        public string City { get; set; }
        public string Street { get; set; }
        public double Quadrature{ get; set; }
        public DateTime PurchaseDate{ get; set; }
        public double Price{ get; set; }

        public FormatedPurchase()
        {
            City = "";
            Street = "";
            Quadrature = 0;
            PurchaseDate = DateTime.Now;
            Price = 0;
        }

        public FormatedPurchase(string city, string street, double quadrature, DateTime purchaseDate, double price)
        {
            City = city;
            Street = street;
            Quadrature = quadrature;
            PurchaseDate = purchaseDate;
            Price = price;
        }


    }
}