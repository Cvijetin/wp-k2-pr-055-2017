﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProdajtaStanova.Web.Models
{
    public class Apartment
    {
        public int ID { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string StreetAndNumber { get; set; }
        public int Flor { get; set; }
        public double Quadrature { get; set; }
        public string Description { get; set; }
        public ApartmentType Type { get; set; }
        public double Price { get; set; }
        public bool OnSale { get; set; }

        public Apartment()
        {
            ID = 0;
            Country = "";
            City = "";
            StreetAndNumber = "";
            Flor = 0;
            Quadrature = 0;
            Description = "";
            Type = ApartmentType.ONE_ROOM;
            Price = 0;
            OnSale = true;
        }

        public Apartment(int id, string country, string city, string streetAndNumber, int flor, double quadrature, string description, ApartmentType type, double price, bool onSale)
        {
            ID = id;
            Country = country;
            City = city;
            StreetAndNumber = streetAndNumber;
            Flor = flor;
            Quadrature = quadrature;
            Description = description;
            Type = type;
            Price = price;
            OnSale = onSale;
        }

        public override string ToString()
        {
            return $"{ID};{Country};{City};{StreetAndNumber};{Flor};{Quadrature};{Description};{Type.ToString()};{Price};{OnSale}";
        }
    }
}