﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace ProdajtaStanova.Web.Models
{
    public class Data
    {
        public static Dictionary<int, User> ReadUsers(string path)
        {
            Dictionary<int, User> users = new Dictionary<int, User>();
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split(';');
                User user = new User(int.Parse(tokens[0]), tokens[1], tokens[2], tokens[3], tokens[4],
                    (Gender)Enum.Parse(typeof(Gender),tokens[5]), tokens[6],
                    DateTime.ParseExact(tokens[7], "dd/MM/yyyy", CultureInfo.InvariantCulture),
                    (UserType)Enum.Parse(typeof(UserType), tokens[8]), bool.Parse(tokens[9]));
                users.Add(user.ID, user);
            }
            sr.Close();
            stream.Close();

            return users;
        }

        public static Dictionary<int, Apartment> ReadApartments(string path)
        {
            Dictionary<int, Apartment> apartments = new Dictionary<int, Apartment>();
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split(';');
                Apartment apartment = new Apartment(int.Parse(tokens[0]), tokens[1], tokens[2],
                            tokens[3], int.Parse(tokens[4]), double.Parse(tokens[5]), tokens[6],
                            (ApartmentType)Enum.Parse(typeof(ApartmentType), tokens[7]), double.Parse(tokens[8]),
                            bool.Parse(tokens[9]));
                apartments.Add(apartment.ID, apartment);
            }
            sr.Close();
            stream.Close();

            return apartments;
        }

        public static void SaveUsers (Dictionary<int, User> users)
        {
            string path = HostingEnvironment.MapPath("~/App_Data/Users.txt");
            using (StreamWriter sw = File.CreateText(path))
            {
                foreach (User item in users.Values)
                {
                    sw.WriteLine(item);
                }
            }
        }

        public static void SaveApartments(Dictionary<int, Apartment> apartments)
        {
            string path = HostingEnvironment.MapPath("~/App_Data/Apartments.txt");
            using (StreamWriter sw = File.CreateText(path))
            {
                foreach (Apartment item in apartments.Values)
                {
                    sw.WriteLine(item);
                }
            }
        }


        public static List<Purchase> ReadPurchases()
        {
            string path = HostingEnvironment.MapPath("~/App_Data/Purchases.txt");
            List<Purchase> purchases = new List<Purchase>();
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split(';');
                Purchase purchase = new Purchase(int.Parse(tokens[0]), int.Parse(tokens[1]),
                    DateTime.Parse(tokens[2]), double.Parse(tokens[3]));
                purchases.Add(purchase);
            }
            sr.Close();
            stream.Close();

            return purchases;
        }

        public static void SavePurchases(List<Purchase> purchases)
        {
            string path = HostingEnvironment.MapPath("~/App_Data/Purchases.txt");
            using (StreamWriter sw = File.CreateText(path))
            {
                foreach (Purchase purchase in purchases)
                {
                    sw.WriteLine(purchase);
                }
            }
        }
    }
}