﻿using ProdajtaStanova.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ProdajtaStanova.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Dictionary<int, User> users = Data.ReadUsers("~/App_Data/Users.txt");
            HttpContext.Current.Application["users"] = users;

            Dictionary<int, Apartment> apartments = Data.ReadApartments("~/App_Data/Apartments.txt");
            HttpContext.Current.Application["Apartments"] = apartments;

            List<Purchase> purchases = Data.ReadPurchases();
            HttpContext.Current.Application["Purchases"] = purchases;
        }
    }
}
